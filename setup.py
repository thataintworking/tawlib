# Author: ron
# Created: 8/8/14
# Copyright (c)2014 That Aint Working, All Rights Reserved

from distutils.core import setup

setup(
    name='tawlib',
    version='1.0',
    description="Library of utilities, helpers and whatnot by That Ain't Working.",
    author='Ron Smith',
    author_email='ron.smith@thataintworking.com',
    packages=[
        'thataintworking',
        'thataintworking.barcode',
        'thataintworking.django',
        'thataintworking.flask',
    ],
    requires=[
        'six',
        'django',
        'flask',
        'Pillow',
    ]
)
