# Author: ron
# Created: 12/30/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved

from urllib.request import Request, build_opener
from urllib.error import HTTPError
import base64
from flask import json
from config import MIDDLE_TIER_URL, MIDDLE_TIER_USERID, MIDDLE_TIER_PASSWD

_mtr_auth_token = None


def get_auth_token():
    global _mtr_auth_token
    if _mtr_auth_token:
        try:
            mtr = mtr_request('/auth/mtr/authtoken', authid=_mtr_auth_token)
            _mtr_auth_token = mtr.get('auth_token')
            return _mtr_auth_token
        except HTTPError:
            pass  # cached token has probably expired, fall through and get a new token
    mtr = mtr_request('/auth/mtr/authtoken', authid=MIDDLE_TIER_USERID, pwd=MIDDLE_TIER_PASSWD)
    _mtr_auth_token = mtr.get('auth_token')
    return _mtr_auth_token


def mtr_request(path, data=None, authid=None, pwd=''):
    if not data: data = {}
    paramstr = '&'.join(['%s=%s' % i for i in data.items()])
    req = Request(MIDDLE_TIER_URL + path, paramstr)
    if not authid:
        authid = get_auth_token()
    if authid:
        req.add_header('Authorization', 'Basic ' + base64.b64encode(authid + ':' + pwd))
    opener = build_opener()
    o = opener.open(req)
    return json.load(o)
