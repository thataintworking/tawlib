# Author: ron
# Created: 2/17/14
# Copyright (c)That Aint Working, All Rights Reserved

from config import CACHE_SERVERS, CACHE_PREFIX, CACHE_TIMEOUT
from memcache import Client


class Cache(object):

    _cache = Client(CACHE_SERVERS)

    def get(self, key):
        return self._cache.get(CACHE_PREFIX + key)

    def set(self, key, val, timeout=CACHE_TIMEOUT):
        self._cache.set(CACHE_PREFIX + key, val, timeout)

    def clear(self):
        self._cache.flush_all()


CACHE = Cache()
