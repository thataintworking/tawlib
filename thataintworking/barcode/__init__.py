# Author: ron
# Created: 3/13/14
# Copyright (c)That Aint Working, All Rights Reserved

__all__ = ['code128b']


class InvalidCharacterError(Exception):
    pass


class UnsuppordedBarcodeSystem(Exception):
    pass


def get_barcode_builder(system):
    if system == 'code128b':
        from . import code128b
        return code128b.create_barcode_image

    # add other barcode systems here

    else:
        raise UnsuppordedBarcodeSystem('Barcode system "%s" is not currently supported.' % system)
