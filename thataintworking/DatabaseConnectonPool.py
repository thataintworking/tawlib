# Author: ron
# Created: 6/26/14
# Copyright (c)2014 That Aint Working, All Rights Reserved

import logging
from time import perf_counter
from queue import Queue
from threading import RLock

logger = logging.getLogger(__name__)


class ConnectionPool(object):
    """
    A simple thread-safe connection pool that just works.
    """

    def __init__(self, driver, userid, passwd, dsn, size):
        """
        :param driver:  The class or module that has the connect() function.
        :param userid:  The userid that will be used to create each connection.
        :param passwd:  The password that will be used to create each connection.
        :param dsn:     The database connect string.
        :param size:    The number of connections to keep in the pool.
        """
        self.driver = driver
        self.userid = userid
        self.passwd = passwd
        self.dsn = dsn
        self.size = size
        self._lock = RLock()
        self._pool = None
        self.reconnect()

    def __del__(self):
        while not self._pool.empty():
            self._pool.get().close()
        self._pool = None

    # noinspection PyProtectedMember
    def get_connection(self, block=True, timeout=None):
        t1 = perf_counter()
        conn = ConnectionPool._Connection(self, block, timeout)
        logger.debug('Got connection from pool in %.3f seconds.', (perf_counter() - t1))
        return conn

    def reconnect(self):
        old_pool = self._pool
        with self._lock:
            self._pool = Queue()
            for i in range(self.size):
                self._pool.put(self.driver.connect(self.userid, self.passwd, self.dsn))
        if old_pool:
            while not old_pool.empty():
                old_pool.get().close()

    # noinspection PyPep8Naming,PyUnusedLocal
    class _Connection(object):
        """
        A duck-type wrapper for the driver's connection class that returns connections to the pool instead of closing them.
        Made this an inner class of the ConnectionPool because it will be accessing _private members of ConnectionPool.
        """

        # noinspection PyProtectedMember
        def __init__(self, cpool, block=True, timeout=None):
            self._cpool = cpool
            with cpool._lock:
                self._pool = cpool._pool
                self._connection = cpool._pool.get(block, timeout)
            self._start_counter = perf_counter()

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            self.close()

        def __del__(self):
            self.close()

        @property
        def action(self):
            return self._connection.action

        @action.setter
        def action(self, value):
            self._connection.action = value

        @property
        def autocommit(self):
            return self._connection.autocommit

        @autocommit.setter
        def autocommit(self, value):
            self._connection.autocommit = value

        def begin(self):
            self._connection.begin()

        def cancel(self):
            self._connection.cancel()

        def changepassword(self, oldpass, newpass):
            self._connection.changepassword(oldpass, newpass)

        @property
        def client_identifier(self):
            return self._connection.client_identifier

        @client_identifier.setter
        def client_identifier(self, value):
            self._connection.client_identifier = value

        @property
        def clientinfo(self):
            return self._connection.clientinfo

        @clientinfo.setter
        def clientinfo(self, value):
            self._connection.clientinfo = value

        # noinspection PyProtectedMember
        def close(self):
            if self._connection:
                logger.debug('Returning connection to pool after %.3f seconds.', (perf_counter() - self._start_counter))
                with self._cpool._lock:
                    if self._pool is self._cpool._pool:
                        self._pool.put(self._connection)
                        self._close()
                        return  # release the lock
                self._connection.close()
                self._close()

        def _close(self):
            self._connection = None
            self._pool = None
            self._cpool = None

        def commit(self):
            self._connection.commit()

        @property
        def current_schema(self):
            return self._connection.current_schema

        @current_schema.setter
        def current_schema(self, value):
            self._connection.current_schema = value

        def cursor(self):
            return self._connection.cursor()

        @property
        def dsn(self):
            return self._connection.dsn

        @property
        def encoding(self):
            return self._connection.encoding

        @property
        def inputtypehandler(self):
            return self._connection.inputtypehandler

        @inputtypehandler.setter
        def inputtypehandler(self, value):
            self._connection.inputtypehandler = value

        @property
        def maxBytesPerCharacter(self):
            return self._connection.maxBytesPerCharacter

        @property
        def module(self):
            return self._connection.module

        @module.setter
        def module(self, value):
            self._connection.module = value

        @property
        def nencoding(self):
            return self._connection.nencoding

        @property
        def outputtypehandler(self):
            return self._connection.outputtypehandler

        @outputtypehandler.setter
        def outputtypehandler(self, value):
            self._connection.outputtypehandler = value

        @property
        def password(self):
            return self._connection.password

        @password.setter
        def password(self, value):
            self._connection.password = value

        def ping(self):
            return self._connection.ping()

        def prepare(self):
            return self._connection.prepare()

        def register(self, code, when, function):
            self._connection.register(code, when, function)

        def rollback(self):
            self._connection.rollback()

        def shutdown(self, mode=None):
            self._connection.shutdown(mode)

        def startup(self, force=False, restrict=False):
            self._connection.startup(force, restrict)

        @property
        def stmtcachesize(self):
            return self._connection.stmtcachesize

        @stmtcachesize.setter
        def stmtcachesize(self, value):
            self._connection.stmtcachesize = value

        def subscribe(self, namespace=cx_Oracle.SUBSCR_NAMESPACE_DBCHANGE, protocol=cx_Oracle.SUBSCR_PROTO_OCI,
                      callback=None, timeout=0, operations=cx_Oracle.OPCODE_ALLOPS, rowids=False, port=0, qos=0, cqqos=0):
            return self._connection.subscribe(namespace, protocol, callback, timeout, operations, rowids, port, qos, cqqos)

        @property
        def tnsentry(self):
            return self._connection.tnsentry

        def unregister(self, code, when):
            self._connection.unregister(code, when)

        @property
        def username(self):
            return self._connection.username

        @property
        def version(self):
            return self._connection.version
