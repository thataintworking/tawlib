# Author: ron
# Created: 2/10/14
# Copyright (c)That Aint Working, All Rights Reserved

import six
from datetime import date, datetime, timedelta
from django import template

register = template.Library()


@register.filter
def adjdate(value, days):
    """Adjust the given date +/- by the specified number of days."""

    if not value:
        return value

    if isinstance(value, six.string_types):
        dt = datetime.strptime(value, '%m-%d-%Y') + timedelta(days)
        return dt.strftime('%m-%d-%Y')

    if isinstance(value, (date, datetime)):
        return value + timedelta(days)

    raise AssertionError('Value is not a date string or datetime type')


@register.filter
def phonenumber(value, country):
    """
    Format the phone number for the specified country.
    For now, ignoring country and formatting everything USA-style.
    """
    #TODO write me
    return value


@register.filter
def range0(value):
    """Returns a list of numbers 0 thru value-1"""
    return range(int(value))


@register.filter
def range1(value):
    """Returns a list of numbers 1 thru value"""
    return range(1, int(value)+1)


@register.filter
def pagecount6(label_count):
    """Returns the number of pages required to print the specified number of labels on 2x3 label sheets (6/sheet)"""
    return int((int(label_count)-1)/6+1)


@register.filter
def labelnum6(page, row):
    """Calculates a label number for the specified page and row on 2x3 label sheets (6/sheet)"""
    return int(page)*6 + int(row)*2

