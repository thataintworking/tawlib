# Author: ron
# Created: 12/23/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved

from urllib.request import Request, build_opener, urlopen
from urllib.error import HTTPError
import base64
from django.conf import settings
from django.core.cache import cache
import json
import logging

logger = logging.getLogger(__name__)

_mtr_auth_token = None


def get_auth_token():
    global _mtr_auth_token
    if _mtr_auth_token:
        try:
            mtr = mtr_post('/auth/mtr/authtoken', authid=_mtr_auth_token)
            _mtr_auth_token = mtr.get('auth_token')
            return _mtr_auth_token
        except HTTPError:
            pass  # cached token has probably expired, fall through and get a new token
    mtr = mtr_post('/auth/mtr/authtoken', authid=settings.MIDDLE_TIER_USERID, pwd=settings.MIDDLE_TIER_PASSWD)
    _mtr_auth_token = mtr.get('auth_token')
    return _mtr_auth_token


def encode_auth(authid, pwd):
    """
    :param authid:  basic_auth userid
    :param pwd:     basic_auth password
    :return:        base64 encoded string with "authid:pwd" suitable for HTTP request header.
    """
    return base64.b64encode((authid + ':' + pwd).encode('utf-8')).decode('utf-8')


def mtr_post(path, data=None, authid=None, pwd=''):
    """
    Gets/puts data from/to the middle-tier.

    :param path:        Appended to the base URL for the middle-tier server.
    :param data:        The data dictionaly passed to the middle-tier.
    :param authid:      Can either be a basic_auth username or an auth_token.
    :param pwd:         Password for basic_auth. Should be an empty string for auth_token.
    :return:            The requested value (usually JSON).
    """
    if not data: data = {}
    req_data = '&'.join(['%s=%s' % i for i in data.items()]).encode('utf-8')
    req = Request(url=settings.MIDDLE_TIER_URL + path, data=req_data)
    if not authid:
        authid = get_auth_token()
    if authid:
        req.add_header('Authorization', 'Basic ' + encode_auth(authid, pwd))
    opener = build_opener()
    f = opener.open(req)
    s = f.read().decode('utf-8')
    return json.loads(s)


def mtr_post_json(path, jsondata, authid=None, pwd=''):
    """
    Gets/puts data from/to the middle-tier using JSON data instead of the usual dictionary.

    :param path:        Appended to the base URL for the middle-tier server.
    :param jsondata:    The data passed to the middle-tier.
    :param authid:      Can either be a basic_auth username or an auth_token.
    :param pwd:         Password for basic_auth. Should be an empty string for auth_token.
    :return:            The requested value (usually JSON).
    """
    req = Request(settings.MIDDLE_TIER_URL + path)
    req.add_header('Content-Type', 'application/json')
    if not authid:
        authid = get_auth_token()
    if authid:
        req.add_header('Authorization', 'Basic ' + encode_auth(authid, pwd))
    resp = urlopen(url=req, data=jsondata.encode('utf-8'))
    s = resp.readall().decode('utf-8')
    return json.loads(s)


def cached_mtr_post(path, authid=None, pwd='', timeout=None, transform=None):
    """
    Gets data from the middle-tier using a cache so it doesn't have to go across the network as often.
    Cannot easily cache results that depend on a dictionary of data so this method doesn't accept that parameter.

    :param path:        Appended to the base URL for the middle-tier server.
    :param authid:      Can either be a basic_auth username or an auth_token.
    :param pwd:         Password for basic_auth. Should be an empty string for auth_token.
    :param timeout:     Specifies the cache timeout that will be set only if the requested value is not already cached.
    :param transform:   A function that is called only if the requested value is not already cached. It takes the value
                        returned from the middle-tier and returns a value that is used instead -- usually just modifying
                        the original.
    :return:            The requested value (usually JSON).
    """
    logger.debug('Getting %s from cache', path)
    mtr = cache.get(path)
    if not mtr:
        logger.debug('%s was not cached. Getting billto info from middle tier.', path)
        mtr = mtr_post(path, authid=authid, pwd=pwd)
        if transform:
            mtr = transform(mtr)
        if timeout:
            cache.set(path, mtr, timeout)
        else:
            cache.set(path, mtr)  # uses default cache timeout from settings.py
    return mtr

