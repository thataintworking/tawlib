# Author: ron
# Created: 12/23/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved

from django.contrib.auth.decorators import user_passes_test


def any_permission_required(*args):
    """
    A decorator which checks user has any of the given permissions.
    permission required can not be used in its place as that takes only a
    single permission.
    """
    def test_func(user):
        for perm in args:
            if user.has_perm(perm):
                return True
        return False
    return user_passes_test(test_func)
