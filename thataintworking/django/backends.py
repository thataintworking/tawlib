# Author: ron
# Created: 12/16/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved

import ldap3 as ldap
import logging
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.backends import ModelBackend

logger = logging.getLogger(__name__)


class LDAPAuthBackend(ModelBackend):
    """ Authenticate against a corporate LDAP. """

    def authenticate(self, username=None, password=None, **kwargs):
        if username and password:
            try:
                ldcon = ldap.initialize(settings.LDAP_URI)
                ldcon.simple_bind_s('%s@dallashdq' % username, password)
                try:
                    user = User.objects.get(username=username)
                except User.DoesNotExist:
                    user = User(username=username, password='FROM LDAP')
                    # user.first_name = #from ldap?
                    # user.last_name = #from ldap?
                    # user.email = #from ldap?
                    # user.is_active = #from ldap?
                    user.is_staff = False
                    user.is_superuser = False
                    user.save()
                return user
            except:
                logger.error('User Authentication Error.', exc_info=True)
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

