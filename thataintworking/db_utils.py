# Author: ron
# Created: 12/23/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved


def dictfetchall(cursor):
    """Returns all rows from a cursor as a dict. This should be used with caution as it could return a lot of data."""
    all_rows = cursor.fetchall()
    if not all_rows:
        return []
    desc = cursor.description
    return [
        dict(zip([col[0].lower() for col in desc], row)) for row in all_rows
    ]


def dictfetchone(cursor):
    """Returns one rows from a cursor as a dict"""
    row = cursor.fetchone()
    if not row:
        return {}
    desc = cursor.description
    return dict(zip([col[0].lower() for col in desc], row))
