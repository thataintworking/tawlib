# Author: ron
# Created: 12/23/13
# Copyright (c)2013 (c)That Aint Working, All Rights Reserved

import functools
from hashlib import md5
import logging
from dateutil import parser

_STANDARD_DATE_TEMPLATE = '%m-%d-%Y'


def standardize_date(date_str):
    if date_str:
        return parser.parse(date_str)
    else:
        return None


def standardize_date_str(date_str):
    dt = standardize_date(date_str)
    if dt:
        return dt.strftime(_STANDARD_DATE_TEMPLATE)
    return dt


def hash_password(cleartext):
    pw_hash = md5()
    pw_hash.update(cleartext.encode('utf-8'))
    return pw_hash.hexdigest().upper()


def trace(logger, level=logging.DEBUG):
    def actual_decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            logger.log(level, 'ENTERING: %s', f.__name__)
            result = f(*args, **kwargs)
            logger.log(level, 'EXITING:  %s', f.__name__)
            return result
        return wrapper
    return actual_decorator
