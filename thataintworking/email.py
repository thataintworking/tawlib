# Author: ron
# Created: 4/21/14
# Copyright (c)That Aint Working, All Rights Reserved

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class RequiredDataException(Exception):
    pass


class Attachment:

    def __init__(self, text, filename):
        self.mime_text = MIMEText(text)
        self.mime_text.add_header('Content-Disposition', 'attachment', filename=filename)


class MailServer:

    def __init__(self, host, default_from=None):
        if not host:
            raise RequiredDataException('host is required')
        self.host = host
        self.default_from = default_from

    def sendmail(self, to, frm=None, subj='', text=None, html=None, attachments=[]):
        if not to:
            raise RequiredDataException('to is require')

        if not frm:
            frm = self.default_from
        if not frm:
            raise RequiredDataException('frm is required if def_frm was not set')

        if text and html:
            msg = MIMEMultipart('alternative')
            msg.attach(MIMEText(text, 'plain'))
            msg.attach(MIMEText(html, 'html'))
        elif text:
            msg = MIMEText(text, 'plain')
        elif html:
            msg = MIMEText(html, 'html')
        else:
            raise RequiredDataException('At least one of text or html is required.')

        if attachments:
            for a in attachments:
                msg.attach(a.mime_text)

        msg['Subject'] = subj
        msg['From'] = frm

        msg['To'] = to
        s = smtplib.SMTP(self.host)
        s.sendmail(frm, to, msg.as_string())
        s.quit()
