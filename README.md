# README #

This is just a library of stuff I use for my projects. If it helps someone else, that's cool.

**No Warranty.** The Software is provided "as is" without warranty of any kind, either express or implied, including without limitation any implied warranties of condition, uninterrupted use, merchantability, fitness for a particular purpose, or non-infringement.